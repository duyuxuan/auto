package com.yj.auto;

import java.util.Date;

import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;

public class Constants {
	// 配置文件名
	public static final String CONF_CONFIG = "conf/config.properties";// 主配置
	public final static Prop config = PropKit.use(Constants.CONF_CONFIG);

	public static final String CONF_JDBC = "conf/jdbc.properties";// 数据库配置
	public static final String CONF_EHCACHE = "conf/ehcache.xml";// 缓存配置
	public static final String CONF_LOG4J = "conf/log4j.xml";// 日志配置
	public static final String CONF_QUARTZ = "conf/quartz.properties";// 调度任务配置
	public final static String DATABASE_NAME = "auto";
	// 字符编码
	public final static String SYS_ENCODING = CONFIG("SYS_ENCODING", "UTF-8");
	public static final String SYS_NAME = CONFIG("SYS_NAME");
	public static final String SYS_ASSET_VERSION = CONFIG("SYS_ASSET_VERSION");
	public static final String SYS_COPYRIGHT = CONFIG("SYS_COPYRIGHT");

	public static final String ACCESS_CHANNEL_ID_KEY = "_ACCESS_CHANNEL_ID_";
	public static final String ACCESS_PLATFORM_KEY = "_ACCESS_PLATFORM_";
	public static final String ONLINE_USER_LISTENER_KEY = "_ONLINE_USER_LISTENER_";
	public static final String SYS_SESSION_USER = "SESSION_USER";
	// 开发模式
	public static final Boolean SYS_DEV_MODE = CONFIG_BOOLEAN("SYS_DEV_MODE");
	public static final Boolean SYS_SAVE_ACCESS_LOG = CONFIG_BOOLEAN("SYS_SAVE_ACCESS_LOG");
	public static final String SYS_INDEX_URI = CONFIG("SYS_INDEX_URI", "index");
	public static final String SYS_COOKIE_KEY = CONFIG("SYS_COOKIE_KEY", "JSESSIONID");
	public static final String SYS_IS_MOBILE = CONFIG("SYS_IS_MOBILE");
	public static final String SYS_VIEW_ROOT = CONFIG("SYS_VIEW_ROOT");
	public static final String SYS_LOG_PATH = CONFIG("SYS_LOG_PATH");
	public static final String SYS_BASE_PATH = CONFIG("SYS_BASE_PATH");
	public static final String SYS_CONTEXT_PATH = CONFIG("SYS_CONTEXT_PATH");
	public static final String SYS_CURRENT_PATH = CONFIG("SYS_CURRENT_PATH");
	public static final String SYS_UPLOAD_ROOT = CONFIG("SYS_UPLOAD_ROOT");
	public static final String SYS_TEMP_FILE_ROOT = CONFIG("SYS_TEMP_FILE_ROOT");
	public static final String SYS_DOWNLOAD_ROOT = CONFIG("SYS_DOWNLOAD_ROOT");
	public static final String OP_MSG_SUCCESS = CONFIG("OP_MSG_SUCCESS");
	public static final String OP_MSG_FAIL = CONFIG("OP_MSG_FAIL");

	public static final Integer TREE_ROOT_ID = 0;// 树形结构顶级节点

	public static final Integer MODEL_SORT_DEFAULT = 10;// 默认排序号

	public static final String DEFAULT_PASSWORD = "666666";// 默认密码

	public static final String DATA_STATE_VALID = "01";// 有效数据状态

	public static final String DATA_STATE_INVALID = "02";// 无效数据状态

	public static final Integer ROLE_ID_SUPER_USER = 1;// 超级用户角色ID

	public static final Integer ROLE_ID_ADMIN_USER = 2;// 系统管理员角色ID

	public static final String MODEL_ATTA_MAP = "_ATTA_MAP_";// 附件上传Model key
	public static final String ATTA_DOWNLOAD_ACTION = "atta/download/";// 附件下载Action
	public static final String USER_DEFAULT_PHOTO = CONFIG("USER_DEFAULT_PHOTO");// 用户默认头像

	// 默认分页：每页显示数量
	public static final Integer PAGE_SIZE = CONFIG_INT("PAGE_SIZE");
	public static final String PAGE_401 = CONFIG("PAGE_401");
	public static final String PAGE_403 = CONFIG("PAGE_403");
	public static final String PAGE_404 = CONFIG("PAGE_404");
	public static final String PAGE_500 = CONFIG("PAGE_500");
	public static final String PAGE_MSG = CONFIG("PAGE_MSG");
	public static final String PAGE_TRANS = CONFIG("PAGE_TRANS");

	// 验证提醒
	public static final String VALID_NOT_NULL = "请填写必填项目";
	public static final String VALID_MAX_LEN = "最多只能输入 %s个字符";

	// 常用权限编号
	public static final String AUTH_CODE_ALL = "all";// 所有权限
	public static final String AUTH_CODE_ADD = "add";// 新增权限编号
	public static final String AUTH_CODE_EDIT = "edit";// 编辑权限编号
	public static final String AUTH_CODE_DEL = "del";// 删除权限编号
	// 缓存名称
	public static final String CACHE_STATE_WAITTING = "_CACHE_STATE_WAITTING_";// 缓存待加载状态
	public static final String CACHE_NAME_DEFAULT = "default";// 默认缓存缓存名称
	public static final String CACHE_NAME_CONFIG = "sys_config";// 配置参数缓存名称
	public static final String CACHE_NAME_DICT = "sys_dict";// 数据字典缓存名称
	public static final String CACHE_NAME_AREA = "sys_area";// 区域缓存名称
	public static final String CACHE_NAME_RESOURCE = "sys_resource";// 菜单资源缓存名称
	public static final String CACHE_NAME_ROLE = "sys_role";// 系统角色缓存名称

	public static String CONFIG(String key) {
		return config.get(key);
	}

	public static String CONFIG(String key, String defaultVal) {
		String val = config.get(key);
		if (StrKit.isBlank(val)) {
			val = defaultVal;
		}
		return val;
	}

	public static Boolean CONFIG_BOOLEAN(String key) {
		return config.getBoolean(key);
	}

	public static Integer CONFIG_INT(String key) {
		return config.getInt(key);
	}

	// 常量方法
	// 是否有效数据
	public static final boolean IS_VALID_DATA(String state) {
		return DATA_STATE_VALID.equals(state);
	}

	// 当前日期
	public static final Date NOW() {
		return new Date();
	}
}
