package com.yj.auto.helper;

import com.yj.auto.component.base.service.DocService;
import com.yj.auto.component.base.service.DocTypeService;
import com.yj.auto.component.base.service.FeedbackService;
import com.yj.auto.component.base.service.NoticeService;

public class ComponentHelper {
	public static NoticeService getNoticeService() {
		return AutoHelper.getService("noticeSrv");
	}

	public static DocTypeService getDocTypeService() {
		return AutoHelper.getService("docTypeSrv");
	}

	public static DocService getDocService() {
		return AutoHelper.getService("docSrv");
	}

	public static FeedbackService getFeedbackService() {
		return AutoHelper.getService("feedbackSrv");
	}
}
