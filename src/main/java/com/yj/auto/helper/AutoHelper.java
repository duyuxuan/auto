package com.yj.auto.helper;

import com.yj.auto.core.jfinal.base.BaseService;
import com.yj.auto.core.jfinal.context.AutoServiceConfig;
import com.yj.auto.core.web.system.service.AttaService;
import com.yj.auto.core.web.system.service.ConfigService;
import com.yj.auto.core.web.system.service.DictService;
import com.yj.auto.core.web.system.service.OrgService;
import com.yj.auto.core.web.system.service.ResourceService;
import com.yj.auto.core.web.system.service.RoleService;
import com.yj.auto.core.web.system.service.ScheduleService;
import com.yj.auto.core.web.system.service.UserService;

public class AutoHelper {
	public static <T extends BaseService> T getService(String serviceName) {
		return (T) AutoServiceConfig.getService(serviceName);
	}

	public static AttaService getAttaService() {
		return getService("attaSrv");
	}

	public static ResourceService getResourceService() {
		return getService("resourceSrv");
	}

	public static RoleService getRoleService() {
		return getService("roleSrv");
	}

	public static DictService getDictService() {
		return getService("dictSrv");
	}

	public static ConfigService getConfigService() {
		return getService("configSrv");
	}

	public static ScheduleService getScheduleService() {
		return getService("scheduleSrv");
	}

	public static OrgService getOrgService() {
		return getService("orgSrv");
	}

	public static UserService getUserService() {
		return getService("userSrv");
	}
}
