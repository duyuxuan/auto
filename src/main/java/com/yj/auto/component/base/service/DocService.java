package com.yj.auto.component.base.service;

import com.jfinal.kit.*;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.*;
import com.yj.auto.core.base.model.*;
import com.yj.auto.core.jfinal.base.*;
import com.yj.auto.helper.LogHelper;
import com.yj.auto.core.base.annotation.*;
import com.yj.auto.Constants;
import com.yj.auto.component.base.model.*;

/**
 * Doc 管理 描述：
 */
@Service(name = "docSrv")
public class DocService extends BaseService<Doc> {

	private static final Log logger = Log.getLog(DocService.class);

	public static final String SQL_LIST = "component.doc.list";
	public static final String ATTA_DOC = "doc_atta";
	public static final Doc dao = new Doc().dao();

	@Override
	public Doc getDao() {
		return dao;
	}

	@Override
	public SqlPara getListSqlPara(QueryModel query) {
		if (StrKit.isBlank(query.getOrderby())) {
			query.setOrderby("sort");
		}
		return getSqlPara(SQL_LIST, query);
	}

	public boolean delete(Integer... ids) {
		LogHelper.delReadingLog(dao.getTableName(), ids);
		return super.delete(ids, ATTA_DOC);
	}
}