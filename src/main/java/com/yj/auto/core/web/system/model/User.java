package com.yj.auto.core.web.system.model;

import java.util.List;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.web.system.model.bean.*;

/**
 * 系统用户
 */
@SuppressWarnings("serial")
@Table(name = User.TABLE_NAME, key = User.TABLE_PK, remark = User.TABLE_REMARK)
public class User extends UserEntity<User> {

	private Org org;

	private List<UserRole> roles;

	public Org getOrg() {
		return org;
	}

	public void setOrg(Org org) {
		this.org = org;
	}

	public List<UserRole> getRoles() {
		return roles;
	}

	public void setRoles(List<UserRole> roles) {
		this.roles = roles;
	}

}