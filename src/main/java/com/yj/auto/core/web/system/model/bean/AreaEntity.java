package com.yj.auto.core.web.system.model.bean;

import java.util.Date;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;

import com.yj.auto.core.jfinal.base.*;

/**
 *  区域管理
 */
@SuppressWarnings("serial")
public abstract class AreaEntity<M extends AreaEntity<M>> extends BaseEntity<M> {
	
	public static final String TABLE_NAME = "t_sys_area"; //数据表名称
	
	public static final String TABLE_PK = "id"; //数据表主键
	
	public static final String TABLE_REMARK = "区域管理"; //数据表备注

	public String getTableName(){
		return TABLE_NAME;
	}

	public String getTableRemark(){
		return TABLE_REMARK;
	}
	
	public String getTablePK(){
		return TABLE_PK;
	}
	
   		
	/**
	 * Column ：id
	 * @return 主键
	 */
   		
	public Integer getId(){
   		return get("id");
   	}
	
	public void setId(Integer id){
   		set("id" , id);
   	}	
   		
	/**
	 * Column ：code
	 * @return 区域编码
	 */
   	@NotBlank
	@Length(max = 32)	
	public String getCode(){
   		return get("code");
   	}
	
	public void setCode(String code){
   		set("code" , code);
   	}	
   		
	/**
	 * Column ：phone
	 * @return 电话区号
	 */
   	@Length(max = 32)	
	public String getPhone(){
   		return get("phone");
   	}
	
	public void setPhone(String phone){
   		set("phone" , phone);
   	}	
   		
	/**
	 * Column ：zip
	 * @return 邮政编码
	 */
   	@Length(max = 32)	
	public String getZip(){
   		return get("zip");
   	}
	
	public void setZip(String zip){
   		set("zip" , zip);
   	}	
   		
	/**
	 * Column ：name
	 * @return 区域名称
	 */
   	@NotBlank
	@Length(max = 64)	
	public String getName(){
   		return get("name");
   	}
	
	public void setName(String name){
   		set("name" , name);
   	}	
   		
	/**
	 * Column ：ename
	 * @return 区域英文名称
	 */
   	@Length(max = 64)	
	public String getEname(){
   		return get("ename");
   	}
	
	public void setEname(String ename){
   		set("ename" , ename);
   	}	
   		
	/**
	 * Column ：spell
	 * @return 区域拼音
	 */
   	@Length(max = 64)	
	public String getSpell(){
   		return get("spell");
   	}
	
	public void setSpell(String spell){
   		set("spell" , spell);
   	}	
   		
	/**
	 * Column ：path
	 * @return 区域路径
	 */
   	@Length(max = 64)	
	public String getPath(){
   		return get("path");
   	}
	
	public void setPath(String path){
   		set("path" , path);
   	}	
   		
	/**
	 * Column ：fullname
	 * @return 区域全名称
	 */
   	@Length(max = 256)	
	public String getFullname(){
   		return get("fullname");
   	}
	
	public void setFullname(String fullname){
   		set("fullname" , fullname);
   	}	
   		
	/**
	 * Column ：parent_id
	 * @return 上级区域
	 */
   	@NotNull 	
	public Integer getParentId(){
   		return get("parent_id");
   	}
	
	public void setParentId(Integer parentId){
   		set("parent_id" , parentId);
   	}	
   		
	/**
	 * Column ：hot
	 * @return 是否热点区域
	 */
   	@Length(max = 32)	
	public String getHot(){
   		return get("hot");
   	}
	
	public void setHot(String hot){
   		set("hot" , hot);
   	}	
   		
	/**
	 * Column ：level
	 * @return 区域级别
	 */
   	@Length(max = 32)	
	public String getLevel(){
   		return get("level");
   	}
	
	public void setLevel(String level){
   		set("level" , level);
   	}	
   		
	/**
	 * Column ：state
	 * @return 区域状态
	 */
   	@NotBlank
	@Length(max = 32)	
	public String getState(){
   		return get("state");
   	}
	
	public void setState(String state){
   		set("state" , state);
   	}	
   		
	/**
	 * Column ：sort
	 * @return 排序号
	 */
   	@NotNull 	
	public Integer getSort(){
   		return get("sort");
   	}
	
	public void setSort(Integer sort){
   		set("sort" , sort);
   	}	
   		
	/**
	 * Column ：remark
	 * @return 描述
	 */
   	@Length(max = 1024)	
	public String getRemark(){
   		return get("remark");
   	}
	
	public void setRemark(String remark){
   		set("remark" , remark);
   	}	
   		
	/**
	 * Column ：luser
	 * @return 最后修改人
	 */
   		
	public Integer getLuser(){
   		return get("luser");
   	}
	
	public void setLuser(Integer luser){
   		set("luser" , luser);
   	}	
   		
	/**
	 * Column ：ltime
	 * @return 最后修改时间
	 */
   		
	public Date getLtime(){
   		return get("ltime");
   	}
	
	public void setLtime(Date ltime){
   		set("ltime" , ltime);
   	}	
}