package com.yj.auto.core.web.system.model;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.jfinal.kit.StrKit;
import com.yj.auto.core.base.annotation.Table;
import com.yj.auto.core.web.system.model.bean.ResourceEntity;

/**
 * 系统资源
 */
@SuppressWarnings("serial")
@Table(name = Resource.TABLE_NAME, key = Resource.TABLE_PK, remark = Resource.TABLE_REMARK)
public class Resource extends ResourceEntity<Resource> {
	private boolean isParent = false;
	public List<ResourceAuthModel> auths;// 资源明细

	public List<ResourceAuthModel> getAuths() {
		if (null == auths && StrKit.notBlank(this.getAuth())) {
			this.auths = JSON.parseArray(this.getAuth(), ResourceAuthModel.class);
		}
		return auths;
	}

	public void setAuths(List<ResourceAuthModel> auths) {
		if (null != auths) {
			String json = JSON.toJSONString(auths);
			this.setAuth(json);
		}
		this.auths = auths;
	}

	// 资源编号是否存在
	public boolean existsRes(String code) {
		if (StrKit.isBlank(code) || null == getAuths())
			return true;
		for (ResourceAuthModel am : auths) {
			if (code.equals(am.getCode())) {
				return true;
			}
		}
		return false;
	}

	public boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(boolean isParent) {
		put("isParent", isParent);
		this.isParent = isParent;
	}

}