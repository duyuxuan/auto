;(function($) {
    $.fn.bootstrapValidator.i18n.decimal = $.extend($.fn.bootstrapValidator.i18n.decimal || {}, {
        'default': '请输入有效的数值，允许 %s位小数'
    });

    $.fn.bootstrapValidator.validators.decimal = {
        html5Attributes: {
            message: 'message'
        },

        enableByHtml5: function($field) {
            return ('number' === $field.attr('type')) && ($field.attr('step') !== undefined) && ($field.attr('step') % 1 !== 0);
        },

        /**
         * Validate decimal number
         *
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field Field element
         * @param {Object} options Consist of key:
         * - message: The invalid message
         * @returns {Boolean}
         */
        validate: function(validator, $field, options) {
            if (this.enableByHtml5($field) && $field.get(0).validity && $field.get(0).validity.badInput === true) {
                return false;
            }
debugger
            var value = $field.val();
            if (value === '') {
                return true;
            }
            
            var s="^(\\d{0,7}(\\.\\d{1,3})?)$|^\\d{0,10}$";
            re= new RegExp(s)
            return re.test(value);
        }
    };
}(window.jQuery));