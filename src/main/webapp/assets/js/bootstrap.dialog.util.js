/**
 * 弹出一个uri窗口
 * 
 * @param title
 * @param uri
 * @param datas
 * @param width
 * @param height 
 * @param buttons
 * @param callback
 *            页面加载成功后执行回调函数
 */
function loadUriDialog(title, uri, datas, width, height, buttons, callback) {
	var options = {
		url : uri,
		global : false,
		type : "POST",
		dataType : "html",
		async : false,
		success : function(msg) {
			var options = {
				title : title,
				draggable : true,
				nl2br : false,
				closeByBackdrop : false,
				closeByKeyboard : false,
				message : function(dialog) {
					var html = dialog.getModalFooter();
					if (_.isArray(buttons) && buttons.length < 1)
						$(html).find(".bootstrap-dialog-footer").parent()
								.addClass("not-modal-footer");
					else
						$(html).find(".bootstrap-dialog-footer").parent()
								.removeClass("not-modal-footer");
					return msg;
				}
			}
			if (_.isArray(buttons)) {
				options.buttons = buttons;
			}
			var dialog = BootstrapDialog.show(options);
			if ("auto"==width){
				width="100%";
			}else if (_.isUndefined(width) || null==width){
				width = "55%";
			}
			dialog.getModalDialog().css("width", width);
			setTimeout(function(){new autoEventListener(dialog.getModal(),true);}, 200);
			if (_.isFunction(callback)) {
				callback.call(this, dialog);
			}
			return dialog;
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
            showMsg("Error : " + XMLHttpRequest.status + " , " + XMLHttpRequest.statusText, "error");
        }
	}
	if (_.isObject(datas)) {
		options.data = datas;
	}
	$.ajax(options);
}

/**
 * 确认对话框
 * 
 * @param msg
 * @param callback
 */
function confirmDialog(msg, callback) {
	var dialog = BootstrapDialog.show({
		title : "提示：请确认",
		message : msg,
		buttons : [ {
			label : '确定',
			cssClass : 'btn-primary btn-sm',
			icon : 'fa fa-check-square',
			action : function(dialog) {
				if (_.isFunction(callback)) {
					callback();
				}
				dialog.close();
			}
		}, {
			label : '取消',
			cssClass : 'btn-warning btn-sm',
			icon : 'fa fa-window-close',
			action : function(dialog) {
				dialog.close();
			}
		} ]
	});
	return dialog;
}

/**
 * 弹出一个DIV对话框
 * 
 * @param title
 * @param id
 * @param width
 */
function infoDialog(title, jqueryOrId, width) {
	var options = {
		title : title,
		draggable : true,
		message : function(dialog) {
			var html = dialog.getModalFooter();
			$(html).find(".bootstrap-dialog-footer").parent().addClass(
					"not-modal-footer");
			if (isJQuery(jqueryOrId)) {
				return jqueryOrId.html();
			} else {
				return $(id).html();
			}
		}
	}
	var dialog = BootstrapDialog.show(options);
	if (_.isEmpty(width))
		width = 800;
	dialog.getModalDialog().css("width", width);
	return dialog;
}

/**
 * 提示信息
 * 
 * @param msg
 */
function warningDialog(msg) {
	var dialog = BootstrapDialog.show({
		title : "提示信息",
		message : msg,
		type : BootstrapDialog.TYPE_DANGER,
		buttons : [ {
			label : '确定',
			cssClass : 'btn-primary btn-sm',
			icon : 'fa fa-check-square',
			action : function(dialogItself) {
				dialogItself.close();
			}
		} ]
	});
	return dialog;
}

/**
 * 关闭弹出窗口
 */
function closeDialog(closeDialogId) {
	if (_.isEmpty(closeDialogId)) {
		BootstrapDialog.closeAll();
	} else {
		var dialog = BootstrapDialog.getDialog(closeDialogId);
		if (null != dialog) {
			dialog.close();
		}
	}
}